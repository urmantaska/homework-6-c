#include <iostream> 
#include <string>
#include <cmath>

class Example //First question
{
private:
	int a, b, c;
public:
	int Get()
	{
		return a, b, c;
	}
	void SetABC(int newA, int newB, int newC)
	{
		a = newA;
		b = newB;
		c = newC;
	}
	int sum()
	{
		return a + b + c;
	}
}; //First question
class Vector //Second question
{
public:
	Vector() : x(0), y(0), z(0), i(0), j(0), p(0)
	{}
	Vector(double _x, double _y, double _z, double _i, double _j, double _p) : x(_x), y(_y), z(_z), i(_i), j(_j), p(_p)
	{}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
		std::cout << '\n' << i << ' ' << j << ' ' << p;
	}
	void CalculateVector()
	{
		double n;
		n = sqrt(pow((x-i),2)+ pow((y - j), 2)+ pow((z - p), 2));
		std::cout << '\n' << "Vector length = " << n;
	}
private:
	double x;
	double y;
	double z;
	double i;
	double j;
	double p;
};
int main()
{
	Example temp;             //First question
	temp.SetABC(5, 9, 15);
	std::cout << temp.sum();
	Vector v(10,5,8,15,6,9);                 //Second question
	v.Show();
	v.CalculateVector();
}